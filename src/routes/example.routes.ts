import { Router } from 'express';

import { deleteUser, getAll, getByName, listUser, login, updateUser } from '../controllers/example.controller';

const router = Router();

router.get('/', getAll);

router.get('/find/:nombre',getByName)

router.get('/list',listUser)

router.put('/:id', updateUser)

router.delete('/:id', deleteUser)

router.post('/login',login)

export default router;
