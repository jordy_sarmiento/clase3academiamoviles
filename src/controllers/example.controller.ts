import {Request, Response} from 'express';
import { personas, usuarios } from '../mock'

const getAll = (req: Request, res: Response) => {
    try{
        res.send({
            success: true,
            message: 'Servidor ejecutandose correctamente',
            data: personas
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const getByName = (req: Request, res: Response) => {
    try{
        const { nombre } = req.params;
        const personaFinded = personas.filter((personas) => personas.nombre.toLowerCase() === nombre.toLowerCase());
        if(personaFinded.length === 0){
            return res.send({success: false, message: 'Nombre no encontrado'})
        }
        return res.send({
            success: true,
            message: 'Persona encontrada',
            data: personaFinded
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const login = (req: Request, res: Response) => {
    try{
        const { username, password } = req.body;
        const user = usuarios.some(usuario => usuario.username === username && usuario.password === password);
        if(!user){
            res.send({
                success: false,
                message: 'Usuario no encontrado'
            })
        }
        res.send({
            success: true,
            message: 'Usuario logeado'
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

/* TODO: UPDATE AND DELETE USER */
const updateUser = (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const numberId = Number(id);
        if(isNaN(numberId)){
            res.status(400).send({
                success: false,
                message: 'Invalid id'
            })
        }
        const {_id, ...body} = req.body;
        const index = usuarios.findIndex(usuario => usuario._id === numberId )
        if(index === -1){
            res.send({
                success: false,
                message: `Usuario con ID ${numberId} no encontrado`
            })
        }

        usuarios[index] = {
            ...usuarios[index],
            ...body
        }

        res.send({
            success: true,
            message: 'Usuario actualizado',
            data: usuarios[index]
        })

    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const deleteUser = (req: Request, res: Response) => {
    try{
        const { id } = req.params;
        const numberId = Number(id);
        if(isNaN(numberId)){
            res.status(400).send({
                success: false,
                message: 'Invalid id'
            })
        }
        const index = usuarios.findIndex(usuario => usuario._id === numberId )
        if(index === -1){
            res.send({
                success: false,
                message: `Usuario con ID ${numberId} no encontrado`
            })
        }

        usuarios.splice(index, 1);

        res.send({
            success: true,
            message: `Usuario con ID ${numberId} eliminado!`
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

const listUser = (req: Request, res: Response) => {
    try {
        res.send({
            success: true,
            message: 'Success',
            data: usuarios
        })
    }
    catch(error){
        res.send({
            success: false,
            message: 'Error',
            error
        })
    }
}

export {
    getAll,
    getByName,
    login,
    updateUser,
    deleteUser,
    listUser
}